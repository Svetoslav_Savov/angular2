import { inject, TestBed } from '@angular/core/testing';

import { AppService } from './app.service';

describe('App Service', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({providers: [AppService]});
  });

  it('should ...', inject([AppService], (app) => {
    expect(app.title).toBe('Angular 2');
  }));
});
