import { Component } from '@angular/core';

import { AppService } from './core/services/app.service';

import '../style/app.scss';

@Component({
  selector: 'app', 
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {

  constructor(private app: AppService) {
    // Do something with api
  }
}
